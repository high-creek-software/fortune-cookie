plugins {
    `java-library`
    idea
    kotlin("jvm")
}

group = "io.highcreeksoftware"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("junit", "junit", "4.12")

}
