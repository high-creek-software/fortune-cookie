package io.highcreeksoftware.fortunecookie

import java.lang.Exception
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

class FortuneCookie(val secretKey: String, val encryptKey: String? = null, val algo: String = "HmacSHA256") {

    fun encode(name: String, value: String): String {
        val now = Date().time
        val group = "$name|$now|$value|"

        val groupBytes = group.toByteArray()
        val mac = createHmac(groupBytes)

        val finalBytes = groupBytes.sliceArray("$name|".toByteArray().size until groupBytes.size) + mac

        return Base64.getEncoder().encodeToString(finalBytes)
    }

    fun decode(name: String, value: String): String {

        val decoded = Base64.getDecoder().decode(value)

        val lastPipe = decoded.lastIndexOf('|'.toByte())
        val mac = decoded.sliceArray(lastPipe + 1 until decoded.size)

        val groupBytes = "$name|".toByteArray() + decoded.sliceArray(0 until lastPipe + 1)
        val mac2 = createHmac(groupBytes)

        if(!mac.contentEquals(mac2)) {
            throw Exception("HMAC not equal")
        }

        return groupBytes.toString(Charsets.UTF_8).split("|")[2]
    }

//    private fun encrypt(value: String): String {
//
//    }
//
//    private fun base64Encode(value: String): String {
//
//    }
//
    private fun createHmac(value: ByteArray): ByteArray {
        val mSha256 = Mac.getInstance(algo)
        mSha256.init(SecretKeySpec(secretKey.toByteArray(), algo))
        return mSha256.doFinal(value)
    }
}