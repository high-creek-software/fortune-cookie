package io.highcreeksoftware.fortunecookie

import org.junit.Assert
import org.junit.Test

class FortuneCookieTest {

    @Test
    fun testEncode() {
        val fc = FortuneCookie("abc123")
        val res = fc.encode("thename", "thevalue")

        val decoded = fc.decode("thename", res)

        Assert.assertEquals("thevalue", decoded)
    }

    @Test
    fun testDecode() {
        val fc = FortuneCookie("abc123")
        val value = fc.decode("thename", "MTYwMDU2NzYzNDgxN3x0aGV2YWx1ZXzKR0rZEE3Ui1sJx19Y2VYioDD90sOanaVKAFCKlx4UsQ==")
        Assert.assertEquals("thevalue", value)
    }

}